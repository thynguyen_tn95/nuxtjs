
module.exports = {

  /*
  ** Headers of the page
  */
  head: {
    title: 'ii-deais（イーディアス）｜採用の課題を情報発信で解決します',
    htmlAttrs: {
      lang: 'ja'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Meta description' }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
    script: [
      { src: '/assets/js/jquery-3.3.1.min.js', type: 'text/javascript', body: true },
      { src: '/assets/js/smooth-scroll.min.js', type: 'text/javascript', body: true }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    'reset-css', '@/assets/scss/style.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: ['@nuxtjs/style-resources'],
  styleResources: {
    sass: [
      '~/assets/scss/_variables.scss'
    ]
  },

  /*
  ** Build configuration
  */
  build: {
    extend(config, ctx) { },
    vendor: ['jquery'],
  },
  generate: {
    fallback: true
  },
  server: {
    port: 3000, // default: 3000
    host: '0.0.0.0', // default: localhost
  }
}
